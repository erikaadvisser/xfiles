package org.n1.xfiles.model.domain

class Smoker {

    long id
    String name
    String shortName
    int level
    BigDecimal balance

    String cssColor
    String glyphicon


    static constraints = {
    }

    String toString() {
        return name
    }
}
