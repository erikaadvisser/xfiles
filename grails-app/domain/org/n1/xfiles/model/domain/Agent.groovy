package org.n1.xfiles.model.domain

import org.n1.xfiles.model.Agency

/**
 * This class defines the agent.
 */
class Agent {

    long id
    String ocName
    String name
    Agency agency;

    /** Skills */
    int science
    int persuasion
    int infiltration

    /** The turnOrder of the player 1-6 */
    int turnOrder;

    /** Statusses */
    boolean fired
    boolean hiding

    static constraints = {
        ocName(nullable: true)
    }

    @Override
    public java.lang.String toString() {
        return name;
    }
}
