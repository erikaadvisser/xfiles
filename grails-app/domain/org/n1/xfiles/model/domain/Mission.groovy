package org.n1.xfiles.model.domain

import org.n1.xfiles.model.MissionColor
import org.n1.xfiles.model.MissionResolution
import org.n1.xfiles.model.MissionStatus
import org.n1.xfiles.model.MissionType

class Mission {

    long id
    String name
    MissionStatus status = MissionStatus.INACTIVE
    int missionOrder
    MissionType type = MissionType.STORY

    int fileVp
    int fileBudget
    int exposeVp
    int exposeBudget
    int exposePanicDie
    boolean exposeBackstory

    Smoker smoker
    MissionColor color
    Mission nextMission = null

    MissionResolution resolution
    int resolutionVp
    int resolutionBudget
    int resolutionPanic
    boolean resolutionBackstory
    Agent resolutionAgent1
    Agent resolutionAgent2
    Smoker resolutionSmoker

    static constraints = {
        resolution nullable: true
        resolutionAgent1 nullable: true
        resolutionAgent2 nullable: true
        resolutionSmoker nullable: true
        nextMission nullable: true
        smoker nullable: true
    }
}
