package org.n1.xfiles.model.domain

import org.joda.time.LocalDateTime
import org.n1.xfiles.model.GmType

class Message {

    Long id
    String content
    GmType target
    boolean read = false
    LocalDateTime entryTime = LocalDateTime.now()
    Smoker smoker

    static constraints = {
        smoker nullable: true
    }

    String toString() {
        if (smoker) {
            return "[${smoker.name}] ${content}"
        } else {
            return content;
        }
    }
}
