package org.n1.xfiles

import org.n1.xfiles.model.MissionStatus
import org.n1.xfiles.model.Page
import org.n1.xfiles.model.domain.Mission

import static org.n1.xfiles.model.MissionStatus.ACTIVE
import static org.n1.xfiles.model.MissionStatus.PENDING

class BrokerMissionController {

    MissionService missionService

    def brokerMission() {
        String statusString = (params.containsKey('status')) ? params.status.toUpperCase() : 'ACTIVE';

        def missions
        if (statusString == "ALL") {
            missions = Mission.list()
        } else {
            MissionStatus status = MissionStatus.valueOf(statusString)
            missions = Mission.findAllByStatus(status)
        }

        missions.each() { it.metaClass.actions = defineActions(it) }

        return [page: Page.BROKER_MISSION,
                missions: missions]
    }

    private defineActions(Mission mission) {
        def actions = []
        if (mission.status == ACTIVE) {
            actions.add("complete")
            actions.add("pending")
            actions.add("deactivate")
        } else if (mission.status == PENDING) {
            actions.add("activate")
            actions.add("complete")
            actions.add("deactivate")
        } else {
            actions.add("activate")
        }
        return actions
    }

    def action() {
        long id = params.getLong('id')
        String command = params.command

        if ("activate" == command) {
            missionService.activate(id)
        }
        if ("pending" == command) {
            missionService.pending(id)
        }
        if ("deactivate" == command) {
            missionService.deactivate(id)
        }
        if ("complete" == command) {
            missionService.markCompleted(id)
        }

        redirect controller: "brokerMission", action: "brokerMission"
    }

}
