package org.n1.xfiles

import org.n1.xfiles.model.GmType
import org.n1.xfiles.model.Page

class BoardHomeController {

    TurnService turnService
    MessageService messageService

    def boardHome() {
        return [page: Page.BOARD_HOME,
                agent: turnService.currentPlayer,
                messages: findMessages(),
                turnSeconds: turnService.turnSecondsSoFar(),
                gameSeconds: turnService.gameSecondsSoFar(),
                round: turnService.round,
                paused: turnService.paused]
    }

    private findMessages() {
        def messages = messageService.findMessagesFor(5, GmType.BOARD)

        messages.each() { it.metaClass.command = it.read ? 'unread' : 'read' }
        messages.each() { it.metaClass.icon = it.read ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-user' }
        return messages
    }

    def nextPlayer() {
        turnService.nextPlayer()
        redirect(action: "boardHome")
    }

    def previousPlayer() {
        turnService.previousPlayer()
        redirect(action: "boardHome")
    }

    def pause() {
        turnService.togglePause()
        redirect(action: "boardHome")
    }

    def readMessage() {
        long id = params.long('id')
        messageService.read(id)
        redirect(action: "boardHome")
    }

    def unreadMessage() {
        long id = params.long('id')
        messageService.unread(id)
        redirect(action: "boardHome")
    }

    def missionResolved() {
        flash.message = "Mission resolved."
        turnService.nextPlayer()
        redirect(action: "boardHome")
    }
}
