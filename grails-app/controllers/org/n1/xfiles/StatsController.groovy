package org.n1.xfiles

import org.n1.xfiles.model.Page
import org.n1.xfiles.model.domain.Agent

class StatsController {

    AgentService agentService
    TurnService turnService

    def stats() {

        int agentId = (params.containsKey('id')) ? params.int('id') : turnService.currentPlayer.id
        [page: Page.STATS,
                agents: Agent.list(),
                agent: Agent.findById(agentId)]
    }

    def save(long id) {
        def a = params;
        agentService.updateAgent(id, params);
        redirect(controller: "boardHome", action: "boardHome")
    }
}
