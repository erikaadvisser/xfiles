package org.n1.xfiles

import org.n1.xfiles.model.*
import org.n1.xfiles.model.domain.Agent
import org.n1.xfiles.model.domain.Mission
import org.n1.xfiles.model.domain.Smoker

import static org.n1.xfiles.model.MissionResolution.*

class ActionsController {

    TurnService turnService
    StatsService statsService
    MessageService messageService
    MissionService missionService

    def actions() {
        return [page: Page.ACTIONS]
    }

    def agentCausedPanic() {
        def agent = turnService.currentPlayer
        def agents = Agent.list()

        return [page: Page.ACTIONS,
                agents: agents,
                agent: agent,
                panic: 0,
                location: ""]
    }

    def agentCausedPanicSubmit() {
        int panic = params.getInt("panic")
        String location = ((String) params.get("location")).trim()

        int agentId = params.int('agentId')
        def agent = Agent.findById(agentId)

        if (panic > 0 && !location.isEmpty()) {
            statsService.logAgentCausedPanic(agent, panic, location)
            def message = "${agent.name} caused ${panic} panic near ${location}"
            messageService.createMessage(message, GmType.BROKER)
            flash.message = "Action performed: ${message}"
            redirect(controller: "boardHome", action: "boardHome")
            return
        }

        if (location.isEmpty()) {
            flash.message = "Please enter a location where the panic resulted from."
        }
        if (panic <= 0) {
            flash.message = "Please enter a panic greater than zero"
        }

        def agents = Agent.list()
        render(view: "agentCausedPanic", model: [page: Page.ACTIONS,
                agents: agents,
                agent: agent,
                panic: panic,
                location: location])
    }

    def coverUp() {
        def missions = Mission.findAllByStatusAndType(MissionStatus.ACTIVE, MissionType.STORY)
        Agency agency = turnService.currentPlayer.agency

        return [missions: missions, agency: agency, vp: 0, budget: 0, mission: null]
    }

    def coverUpSubmit() {
        String agencyName = params.get("agency")
        def agency = Agency.findByName(agencyName)

        Long missionId = params.getLong("missionId")
        Mission mission = (missionId) ? Mission.findById(missionId) : null
        int budget = params.getInt("budget")
        int vp = params.getInt("vp")

        if (vp > 0 && budget > 0 && mission) {
            statsService.logCoverUp(agency, mission, vp, budget)
            def message = "The ${agency.name} has covered up ${mission.name}."
            messageService.createMessage(message, mission.smoker, GmType.BROKER)
            flash.message = "Action performed: ${message}"
            redirect(controller: "boardHome", action: "boardHome")
            return
        }
        def missions = Mission.findAllByStatusAndType(MissionStatus.ACTIVE, MissionType.STORY)

        if (vp <= 0) {
            flash.message = "Please enter a positive amount of VP."
        }
        if (budget <= 0) {
            flash.message = "Please enter a positive budget."
        }
        if (!mission) {
            flash.message = "Please choose a mission that was covered up."
        }

        render(view: "coverUp", model: [missions: missions, agency: agency, vp: vp, budget: budget, mission: mission])
        return
    }

    def missionComplete() {
        def agent = turnService.currentPlayer
        def allAgents = Agent.list()

        def missions = Mission.findAllByStatus(MissionStatus.ACTIVE)
        missions.removeAll() { it.type == MissionType.SMOKER }

        return [agents: allAgents, agentsChosen: [agent],
                missions: missions, mission: null,
                vp: 0, panic: 0, resolution: null, blocker: null, backstory: false]
    }

    def missionCompleteSubmit() {
        String resolutionName = params.resolution?.toUpperCase()
        MissionResolution resolution = (resolutionName) ? MissionResolution.valueOf(resolutionName) : null

        Collection<Agent> agents = params.agentId.collect() { Agent.findById(Long.parseLong(it)) }
        Long missionId = params.long("missionId")
        Mission mission = (missionId) ? Mission.findById(missionId) : null
        int vp = params.int("vp")
        int panic = params.int("panic")
        Integer blockerId = params.getInt("blockerId")
        Agent blocker = (blockerId) ? Agent.findById(blockerId) : null
        boolean backstory = params.containsKey("backstory")

        String message = checkParamsMissionComplete(mission, agents, resolution, vp, panic, backstory, blocker)
        if (message != null) {
            flash.message = message

            def allAgents = Agent.list()
            def missions = Mission.findAllByStatus(MissionStatus.ACTIVE)
            missions.removeAll() { it.type == MissionType.SMOKER }

            render(view: "missionComplete",
                    model: [agents: allAgents, agentsChosen: agents,
                            missions: missions, mission: mission,
                            vp: vp, panic: panic, resolution: resolution, blocker: blocker, backstory: backstory])

            return
        }

        message = missionService.complete(mission, agents, resolution, vp, panic, backstory, blocker)
        flash.message = "Action performed: ${message}"
        redirect(controller: "boardHome", action: "boardHome")
    }

    private String checkParamsMissionComplete(Mission mission, List<Agent> agents, MissionResolution resolution, int vp, int panic, boolean backstory, Agent blocker) {
        if (mission == null) {
            return "Choose a mission."
        }
        if (agents.isEmpty()) {
            return "Choose agents."
        }
        if (agents.agency.unique().size() > 1) {
            return "Choose agents from the same agency."
        }
        if (!resolution) {
            return "Choose a resolution"
        }
        if (resolution == EXPOSED) {
            if (vp <= 0 && panic <= 0) {
                return "Choose VP and Panic."
            }
        }
        if (resolution == EXPOSE_PREVENTED) {
            if (blocker == null) {
                return "Select the agent that prevented the exposing."
            }
            if (agents[0].agency == blocker.agency) {
                return "The agent that prevented the exposing is of the same agency as the exposing agents."
            }
        }
        if (resolution != EXPOSED) {
            if (vp != 0 || panic != 0 || backstory) {
                return "VP, backstory and panic values are only allowed on Expose. Please choose a valid combination."
            }
        }

        return null
    }

    /**
     * The three views: missionProcessEvidence, raid and local are virtually identical in the sense of parameters and options.
     */

    enum PageType {
        LOCAL, PROCESS_EVIDENCE, RAID
    }

    def missionProcessEvidence() {
        return genericActionShow(MissionStatus.PENDING, MissionType.STORY)
    }

    def raid() {
        return genericActionShow(MissionStatus.COMPLETED, MissionType.STORY)
    }

    def local() {
        return genericActionShow(MissionStatus.ACTIVE, MissionType.LOCAL)
    }

    def missionProcessEvidenceSubmit() {
        genericActionSubmit(MissionStatus.PENDING, MissionType.STORY, PageType.PROCESS_EVIDENCE)
    }

    def raidSubmit() {
        genericActionSubmit(MissionStatus.COMPLETED, MissionType.STORY, PageType.RAID)
    }

    def localSubmit() {
        genericActionSubmit(MissionStatus.ACTIVE, MissionType.LOCAL, PageType.LOCAL)
    }

    private def genericActionShow(MissionStatus missionSatus, MissionType type) {
        def agent = turnService.currentPlayer
        def allAgents = Agent.list()
        def missions = Mission.findAllByStatusAndType(missionSatus, type)
        def smokers = Smoker.list()

        return [agents: allAgents, agentsChosen: [agent],
                missions: missions, mission: null,
                smokers: smokers, smoker: null,
                vp: 0, budget: 0, resolution: null]

    }

    private def genericActionSubmit(MissionStatus missionSatus, MissionType type, PageType pageType) {
        String resolutionName = params.resolution?.toUpperCase()
        MissionResolution resolution = (resolutionName) ? MissionResolution.valueOf(resolutionName) : null

        Collection<Agent> agents = params.agentId.collect() { Agent.findById(Long.parseLong(it)) }
        Long smokerId = params.getLong("smokerId")
        Smoker smoker = (smokerId) ? Smoker.findById(smokerId) : null
        Long missionId = params.long("missionId")
        Mission mission = (missionId) ? Mission.findById(missionId) : null
        int vp = params.int("vp")
        int budget = params.int("budget")

        String message = checkParamsGenericAction(mission, agents, resolution, vp, budget, smoker, pageType)
        if (message != null) {
            flash.message = message
            def allAgents = Agent.list()
            def missions = Mission.findAllByStatusAndType(missionSatus, type)
            def smokers = Smoker.list()

            String view
            switch (pageType) {
                case PageType.LOCAL:
                    view = "local"
                    break
                case PageType.RAID:
                    view = "raid"
                    break
                case PageType.PROCESS_EVIDENCE:
                    view = "missionProcessEvidence"
            }

            render(view: view, model: [agents: allAgents, agentsChosen: agents,
                    missions: missions, mission: mission,
                    smokers: smokers, smoker: smoker,
                    vp: vp, budget: budget, resolution: resolution])
            return
        }

        switch (pageType) {
            case PageType.LOCAL:
                message = missionService.processLocal(mission, agents, resolution, vp, budget)
                break
            case PageType.RAID:
                message = missionService.raid(mission, agents, resolution, vp, budget, smoker)
                break
            case PageType.PROCESS_EVIDENCE:
                message = missionService.processEvidence(mission, agents, resolution, vp, budget, smoker)
        }

        flash.message = "Action performed: ${message}"
        redirect(controller: "boardHome", action: "boardHome")

    }

    private String checkParamsGenericAction(Mission mission, Collection<Agent> agents,
                                            MissionResolution resolution, int vp, int budget, Smoker smoker, PageType pageType) {
        if (agents.isEmpty()) {
            return "Choose agents."
        }
        if (agents.agency.unique().size() > 1) {
            return "Choose agents from the same agency."
        }
        if (mission == null) {
            return "Choose a mission."
        }
        if (!resolution) {
            return "Choose a resolution"
        }
        if (resolution != FAILED & vp <= 0 && budget <= 0) {
            return "Choose VP and budget."
        }

        if (pageType == PageType.LOCAL) {
            return null;
        }

        if ((resolution == EMBEZZLED || resolution == SUCCESS) && !smoker) {
            return "Choose a smoker."
        }
        return null
    }

}


