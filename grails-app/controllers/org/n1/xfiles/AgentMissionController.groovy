package org.n1.xfiles

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.n1.xfiles.model.MissionResolution
import org.n1.xfiles.model.MissionStatus
import org.n1.xfiles.model.MissionType
import org.n1.xfiles.model.Page
import org.n1.xfiles.model.domain.Agent
import org.n1.xfiles.model.domain.Mission

import static org.n1.xfiles.model.MissionResolution.*

class AgentMissionController {

    TurnService turnService
    MissionService missionService

    def agentMission() {
        def missions = Mission.findAllByStatus(MissionStatus.ACTIVE)

        missions.removeAll() { it.type == MissionType.SMOKER }

        return [page: Page.AGENT_MISSION,
                agent: turnService.currentPlayer,
                agents: Agent.list(),
                missions: missions]
    }

    def save() {
        String errorMessage = validateSaveParams(params)
        if (errorMessage != null) {
            flash.message = errorMessage
            redirect(action: "agentMission")
            return
        }

        MissionResolution resolution = MissionResolution.valueOf(params.resolution.toUpperCase())

        Collection<Agent> agents = params.agent.collect() { Agent.findById(Long.parseLong(it)) }

        boolean backstory = params.hasProperty("backstory")

        def result = missionService.complete(
                params.long("mission"),
                agents,
                resolution,
                params.int("vp"),
                params.int("budget"),
                params.int("panic"),
                backstory,
                params.long("smoker")
        )
        redirect(controller: "boardHome", action: "missionResolved")
    }

    private String validateSaveParams(GrailsParameterMap params) {
        if (params.mission == null) {
            return "File input incomplete: choose a mission."
        }
        Mission mission = Mission.findById(params.long("mission"))
        if (mission.type == MissionType.SMOKER) {
            return "Cannot solve Smoker missions by agents."
        }

        if (params.resolution == null) {
            return "File input incomplete: choose a resolution."
        }
        if (params.agent == null) {
            return "File input incomplete: choose agent(s)."
        }

        MissionResolution resolution = MissionResolution.valueOf(params.resolution.toUpperCase())
        if (resolution == FILED) {
            if (mission.type == MissionType.SMOKER && (params.int("vp") == 0 || params.int("budget") == 0)) {
                return "Input incomplete: choose VP and Budget."
            }
            if (params.boolean("backstory")) {
                return "Input invalid: cannot receive backstory on file."
            }
        }
        if (resolution == EMBEZZLED) {
            if (mission.type == MissionType.SMOKER && (params.int("vp") == 0 && params.int("budget") == 0)) {
                return "Embezzle input incomplete: choose VP and Budget."
            }
            if (params.long("smoker") == null) {
                return "Embezzle input incomplete: choose a smoker to embezzle for."
            }
        }

        if (resolution == EXPOSED) {
            if (params.int("vp") == 0 || params.int("panic") == 0) {
                return "Expose input incomplete: choose VP and Panic."
            }
        }

        Collection<Agent> agents = params.agent.collect() { Agent.findById(Long.parseLong(it)) }
        def agencies = agents.collect() { it.agency }
        if (agencies.unique().size() > 1) {
            return "Cannot file a mission with agents from different agencies."
        }

        return null
    }
}
