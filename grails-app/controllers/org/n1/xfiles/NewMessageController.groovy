package org.n1.xfiles

import org.n1.xfiles.model.GmType

class NewMessageController {

    MessageService messageService

    def newMessageForBroker() {
        if (params.containsKey("content")) {
            messageService.createMessage(params.content, GmType.BROKER)
            redirect controller: "boardHome", action: "boardHome"
            return
        }

        render view: "newMessage", model: [layout: "board",
                command: "newMessageForBroker"]
    }

    def newMessageForBoard() {
        if (params.containsKey("content")) {
            messageService.createMessage(params.content, GmType.BOARD)
            redirect controller: "brokerHome", action: "brokerHome"
            return
        }

        render view: "newMessage", model: [layout: "broker",
                command: "newMessageForBoard"]
    }
}
