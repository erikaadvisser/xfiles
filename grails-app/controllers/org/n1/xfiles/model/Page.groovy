package org.n1.xfiles.model

/**
 * This enum defines the pages that exist in the app.
 */
public enum Page {

    BOARD_HOME("Home", "boardHome", "boardHome", true),
    ACTIONS("Actions", "actions", "actions", true),
    STATS("Stats", "stats", "stats", true),
    BOARD_NEW_MESSAGE("Message", "newMessage", "newMessageForBroker", true),
    BOARD_ADMIN("Admin", "admin", "admin", true),

    BROKER_HOME("Home", "brokerHome", "brokerHome", false),
    BROKER_MISSION("Missions", "brokerMission", "brokerMission", false),
    BROKER_NEW_MESSAGE("Message", "newMessage", "newMessageForBoard", false)

    public final String name;
    public final String controller;
    public final String action;
    public final boolean boardGame;

    Page(String name, String controller, String action, boolean boardGame) {
        this.name = name
        this.controller = controller
        this.action = action
        this.boardGame = boardGame
    }

}