package org.n1.xfiles

import org.n1.xfiles.model.Page
import org.n1.xfiles.model.domain.Agent

class AdminController {

    TurnService turnService

    def admin() {
        def list = Agent.list(sort: "turnOrder")
        [page: Page.BOARD_ADMIN,
                agentInstanceList: list]
    }

    def start() {
        turnService.start()
        redirect action: "boardHome", controller: "boardHome"
    }

    def save() {
        Agent.list().each() {
            String ocNameKey = "agent[${it.id}].ocName"
            it.ocName = params.get(ocNameKey)
            it.turnOrder = params.int("agent[${it.id}].turnOrder")
            it.save(flush: true)
        }
        redirect action: "admin"
    }
}
