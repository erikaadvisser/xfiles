package org.n1.xfiles

import org.n1.xfiles.model.GmType
import org.n1.xfiles.model.Page

class BrokerHomeController {

    MessageService messageService

    def brokerHome() {
        return [page: Page.BROKER_HOME,
                messages: findMessages()]
    }

    private findMessages() {
        def messages = messageService.findMessagesFor(10, GmType.BROKER)

        messages.each() { it.metaClass.command = it.read ? 'unread' : 'read' }
        messages.each() { it.metaClass.icon = it.read ? 'glyphicon glyphicon-ok' : 'glyphicon glyphicon-user' }
        return messages
    }

    def readMessage() {
        long id = params.long('id')
        messageService.read(id)
        redirect(action: "brokerHome")
    }

    def unreadMessage() {
        long id = params.long('id')
        messageService.unread(id)
        redirect(action: "brokerHome")
    }
}
