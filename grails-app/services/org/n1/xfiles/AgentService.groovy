package org.n1.xfiles

import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap
import org.n1.xfiles.model.Skill
import org.n1.xfiles.model.domain.Agent

/**
 * This service manages agents.
 */
class AgentService {

    StatsService statsService

    void updateAgent(long id, GrailsParameterMap params) {
        Agent agent = Agent.findById(id)

        int changeScience = params.int('science') - agent.science
        int changePersuasion = params.int('persuasion') - agent.persuasion
        int changeInfiltration = params.int('infiltration') - agent.infiltration

        agent.properties = params
        agent.save(flush:true)

        if (changeInfiltration) {
            statsService.logStatChange(agent, Skill.INFILTRATION, agent.infiltration, changeInfiltration)
        }
        if (changePersuasion) {
            statsService.logStatChange(agent, Skill.PERSUASION, agent.persuasion, changePersuasion)
        }
        if (changeScience) {
            statsService.logStatChange(agent, Skill.SCIENCE, agent.science, changeScience)
        }
    }
}
