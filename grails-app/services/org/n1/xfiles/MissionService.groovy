package org.n1.xfiles

import grails.transaction.Transactional
import org.n1.xfiles.model.MissionResolution
import org.n1.xfiles.model.MissionStatus
import org.n1.xfiles.model.domain.Agent
import org.n1.xfiles.model.domain.Mission
import org.n1.xfiles.model.domain.Smoker

import static org.n1.xfiles.model.GmType.BOARD
import static org.n1.xfiles.model.GmType.BROKER
import static org.n1.xfiles.model.MissionResolution.*
import static org.n1.xfiles.model.MissionStatus.*
import static org.n1.xfiles.model.MissionType.SMOKER

@Transactional
class MissionService {

    StatsService statsService
    MessageService messageService

    void activate(long id) {
        Mission mission = Mission.findById(id)
        mission.status = ACTIVE
        messageService.createMessage("Broker re-activated mission: ${mission.name}", BOARD)

        statsService.logActivate(mission)
    }

    def pending(long id) {
        Mission mission = Mission.findById(id)
        mission.status = PENDING
        messageService.createMessage("Broker set status to pending for mission: ${mission.name}", BOARD)

        statsService.logPending(mission)
    }


    void deactivate(long id) {
        Mission mission = Mission.findById(id)
        mission.status = INACTIVE
        messageService.createMessage("Broker de-activated mission: ${mission.name}", BOARD)

        statsService.logDeactivate(mission)
    }

    void markCompleted(long id) {
        Mission mission = Mission.findById(id)
        mission.status = COMPLETED
        statsService.logMarkCompleted(mission)
        String message = "Broker completed: ${mission.name}";

        if (mission.type == SMOKER) {
            Mission nextMission = Mission.findBySmokerAndMissionOrder(mission.smoker, mission.missionOrder + 1)
            nextMission.status = ACTIVE
            statsService.logActivate(nextMission)
            message = "Start mission: \"${nextMission.name}\" (-> ${mission.name} mission completed)."
        }
        messageService.createMessage(message, BOARD)
        messageService.createMessage(message, BROKER)
    }


    String complete(Mission mission, Collection<Agent> agents, MissionResolution resolution, int vp, int panic, boolean backstoryReceived, Agent blocker) {
        if (resolution == MissionResolution.FAILED) {
            statsService.logMissionFailed(mission, agents)
            String msg = "${mission.name} attempted by ${agents} but failed."
            messageService.createMessage(msg, mission.smoker, BROKER)
            return msg
        }

        String message = updateStatusAndCreateMessage(resolution, mission, agents, panic, blocker)
        mission.resolution = resolution
        mission.resolutionVp = vp
        mission.resolutionBackstory = backstoryReceived
        mission.resolutionPanic = panic
        mission.resolutionAgent1 = agents[0]
        if (agents.size() == 2) {
            mission.resolutionAgent2 = agents[1]
        }
        statsService.logMissionCompleted(mission, resolution, agents, vp, panic, backstoryReceived)
        messageService.createMessage(message, mission.smoker, BROKER)

        if (mission.nextMission != null) {
            Mission nextMission = mission.nextMission;

            nextMission.status = MissionStatus.ACTIVE;
            messageService.createMessage("Auto start: ${nextMission.name}", BROKER)
            messageService.createMessage("Auto start: ${nextMission.name}", BOARD)
        }
        return message
    }


    String processEvidence(Mission mission, Collection<Agent> agents, MissionResolution resolution, int vp, int budget, Smoker smoker) {
        mission.status = COMPLETED
        mission.resolutionVp = vp
        mission.resolutionBudget = vp;

        statsService.logMissionEvidenceProcessed(mission, resolution, agents, vp, budget, smoker)

        if (resolution == MissionResolution.EMBEZZLED) {
            mission.resolutionBackstory = true
            String message = "${mission.name} embezzled by ${agents} for ${smoker.name}.";
            messageService.createMessage(message, smoker, BROKER)

            return message
        } else if (resolution == MissionResolution.FILED) {
            String message = "${mission.name} filed by ${agents}.";
            messageService.createMessage(message, mission.smoker, BROKER)
            return message
        } else {
            throw new IllegalArgumentException("Unsupported resolution: ${resolution}")
        }
    }


    private String updateStatusAndCreateMessage(MissionResolution resolution, Mission mission, Collection<Agent> agents, int panic, Agent blocker) {
        if (resolution == SUCCESS) {
            mission.status = MissionStatus.PENDING
            return "${mission.name} completed by ${agents}"
        }
        if (resolution == EXPOSED) {
            mission.status = MissionStatus.COMPLETED
            return "${mission.name} exposed by ${agents} for ${panic} panic"
        }
        if (resolution == EXPOSE_PREVENTED) {
            mission.status = MissionStatus.COMPLETED
            return "${mission.name} expose attempted by ${agents} but prevented by ${blocker}"
        }
        throw new IllegalArgumentException("Unexpected resolution: ${resolution}")
    }

    String raid(Mission mission, Collection<Agent> agents, MissionResolution resolution, int vp, int budget, Smoker smoker) {
        statsService.logRaid(mission, resolution, agents, vp, budget, smoker)

        if (resolution == MissionResolution.SUCCESS) {
            String message = "${mission.name} raided by ${agents} for ${smoker.name}.";
            messageService.createMessage(message, smoker, BROKER)

            return message
        } else if (resolution == MissionResolution.FAILED) {
            String message = "Failed to raid ${mission.name} by ${agents}.";
            messageService.createMessage(message, mission.smoker, BROKER)
            return message
        } else {
            throw new IllegalArgumentException("Unsupported resolution: ${resolution}")
        }


    }

    String processLocal(Mission mission, List<Agent> agents, MissionResolution resolution, int vp, int budget) {
        statsService.logLocalMission(mission, resolution, agents, vp, budget)

        if (resolution == MissionResolution.SUCCESS) {
            String message = "Local mission ${mission.name} solved by ${agents}";
            mission.status = MissionStatus.COMPLETED
            messageService.createMessage(message, BROKER)

            Mission nextMission = findNextLocalMission()
            if (nextMission) {
                nextMission.status = MissionStatus.ACTIVE
                messageService.createMessage("Auto start: ${nextMission.name}", BOARD)
            }

            return message
        } else if (resolution == MissionResolution.FAILED) {
            String message = "Local mission ${mission.name} failed by ${agents}.";
            messageService.createMessage(message, BROKER)
            return message
        } else {
            throw new IllegalArgumentException("Unsupported resolution: ${resolution}")
        }
    }

    private Mission findNextLocalMission() {
        def missions = Mission.findAllByStatus(MissionStatus.INACTIVE)

        if (missions.isEmpty()) {
            return null
        }
        Collections.shuffle(missions)
        missions.sort({ a, b -> a.missionOrder <=> b.missionOrder })

        return missions.head()
    }
}
