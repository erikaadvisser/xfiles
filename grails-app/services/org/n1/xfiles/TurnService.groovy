package org.n1.xfiles

import grails.transaction.Transactional
import org.joda.time.LocalDateTime
import org.joda.time.LocalTime
import org.joda.time.Period
import org.n1.xfiles.model.PausableTimer
import org.n1.xfiles.model.domain.Agent

/**
 * This service provides methods for manipulating game turns.
 */
@Transactional
class TurnService {

    StatsService statsService

    int round = 0
    Agent currentPlayer = null;

    boolean paused

    private PausableTimer turnTimer
    private PausableTimer roundTimer
    private PausableTimer gameTimer

    void start() {
        turnTimer = new PausableTimer()
        roundTimer = new PausableTimer()
        gameTimer = new PausableTimer()
        paused = false
        currentPlayer = Agent.findByTurnOrder(6)
        nextPlayer()
    }

    void nextPlayer() {
        statsService.logTurnEnd(currentPlayer, turnSecondsSoFar())
        turnTimer = new PausableTimer()
        roundTimer.unPause()
        gameTimer.unPause()
        paused = false

        int nextOrder = currentPlayer.turnOrder + 1
        if (nextOrder == 7) {
            nextOrder = 1
            roundEnd()
        }
        currentPlayer = Agent.findByTurnOrder(nextOrder)
    }

    private void roundEnd() {
        statsService.roundEnd(round, roundSecondsSoFar())
        roundTimer = new PausableTimer()
        round += 1
    }

    void previousPlayer() {
        statsService.logPrevPlayer(currentPlayer, turnSecondsSoFar())
        turnTimer = new PausableTimer()
        roundTimer.unPause()
        gameTimer.unPause()

        int prevOrder = currentPlayer.turnOrder -1
        if (prevOrder == 0) {
            prevOrder = 6
            statsService.logPrevRound()
            roundTimer = new PausableTimer()
        }
        currentPlayer = Agent.findByTurnOrder(prevOrder)
    }

    int turnSecondsSoFar() {
        return turnTimer.secondsSoFar()
    }

    int roundSecondsSoFar() {
        return roundTimer.secondsSoFar()
    }

    int gameSecondsSoFar() {
        return gameTimer.secondsSoFar()
    }

    void togglePause() {
        turnTimer.togglePause()
        roundTimer.togglePause()
        gameTimer.togglePause()
        paused = !paused
    }
}
