package org.n1.xfiles

import grails.transaction.Transactional
import org.n1.xfiles.model.Agency
import org.n1.xfiles.model.GmType
import org.n1.xfiles.model.MissionResolution
import org.n1.xfiles.model.Skill
import org.n1.xfiles.model.domain.Agent
import org.n1.xfiles.model.domain.Mission
import org.n1.xfiles.model.domain.Smoker
import org.slf4j.Logger
import org.slf4j.LoggerFactory

/**
 * This service logs events for the purpose of gathering statistics for analyzing games. The format is comma separated to allow parsing in Excel.
 */
@Transactional
class StatsService {

    public static final Logger statsLogger = LoggerFactory.getLogger(StatsService.class)

    enum Events {
        TURN, ROUND, GO_BACK_ONE_PLAYER, CROSS_ROUND_BORDER_WITH_GO_BACK_PLAYER, SKILL_CHANGE, MISSION_ACTIVATE, MISSION_PENDING, MISSION_DEACTIVATE, MISSION_MARK_COMPLETED,
        MISSION_FAILED, MISSION_COMPLETED, MISSION_EVIDENCE_PROCESSED, AGENT_CAUSED_PANIC, COVERD_UP_MISSION, MESSAGE, RAID, LOCAL_MISSION
    }

    void logPrevPlayer(Agent agent, int turnSeconds) {
        statsLogger.info("${Events.GO_BACK_ONE_PLAYER},${agent},after,${turnSeconds}")
    }

    void logPrevRound() {
        statsLogger.info("${Events.CROSS_ROUND_BORDER_WITH_GO_BACK_PLAYER}")
    }

    void logTurnEnd(Agent agent, int turnSeconds) {
        statsLogger.info("${Events.ROUND},${agent},took,${turnSeconds}")
    }

    void roundEnd(int turn, int roundSeconds) {
        statsLogger.info("${Events.TURN},${turn},took,${roundSeconds}")
    }

    void logStatChange(Agent agent, Skill skill, int value, int change) {
        statsLogger.info("${Events.SKILL_CHANGE},${agent.name},${skill},${value},${change > 0 ? '+' + change : change}")
    }

    void logActivate(Mission mission) {
        statsLogger.info("${Events.MISSION_ACTIVATE},${mission.name}")
    }

    void logPending(Mission mission) {
        statsLogger.info("${Events.MISSION_PENDING},${mission.name}")
    }

    void logDeactivate(Mission mission) {
        statsLogger.info("${Events.MISSION_DEACTIVATE},${mission.name}")
    }

    void logMarkCompleted(Mission mission) {
        statsLogger.info("${Events.MISSION_MARK_COMPLETED},${mission.name}")
    }

    void logMissionFailed(Mission mission, List<Agent> agents) {
        statsLogger.info("${Events.MISSION_FAILED},${mission.name},${agents}")
    }

    void logMissionCompleted(Mission mission, MissionResolution resolution, List<Agent> agents, int vp, int panic, boolean backstory) {
        statsLogger.info("${Events.MISSION_COMPLETED},${mission.name},${resolution},${agents},${vp},${backstory},${panic}")
    }

    void logMissionEvidenceProcessed(Mission mission, MissionResolution resolution, Collection<Agent> agents, int vp, int budget, Smoker smoker) {
        statsLogger.info("${Events.MISSION_EVIDENCE_PROCESSED},${mission.name},${resolution},${agents},${vp},${budget},${smoker}")
    }


    void logAgentCausedPanic(Agent agent, int panic, String location) {
        statsLogger.info("${Events.AGENT_CAUSED_PANIC},${agent.name},${panic},${location}")
    }

    void logCoverUp(Agency agency, Mission mission, int vp, int budget) {
        statsLogger.info("${Events.COVERD_UP_MISSION},${agency.name},${mission.name},${vp},${budget}")
    }

    void logMessage(String message, GmType gmType, Smoker smoker) {
        statsLogger.info("${Events.MESSAGE},${gmType},${smoker},${message}")
    }

    void logRaid(Mission mission, MissionResolution resolution, Collection<Agent> agents, int vp, int budget, Smoker smoker) {
        statsLogger.info("${Events.RAID},${mission.name},${resolution},${agents},${vp},${budget},${smoker}")
    }

    void logLocalMission(Mission mission, MissionResolution resolution, List<Agent> agents, int vp, int budget) {
        statsLogger.info("${Events.LOCAL_MISSION},${mission.name},${resolution},${agents},${vp},${budget}")
    }


}
