package org.n1.xfiles

import grails.transaction.Transactional
import org.n1.xfiles.model.GmType
import org.n1.xfiles.model.domain.Message
import org.n1.xfiles.model.domain.Smoker

@Transactional
class MessageService {

    StatsService statsService

    void read(long id) {
        Message message = Message.findById(id)
        message.read = true
    }

    void unread(long id) {
        Message message = Message.findById(id)
        message.read = false
    }

    void createMessage(String content, GmType targetGm) {
        createMessage(content, null, targetGm)
    }

    void createMessage(String content, Smoker smoker, GmType targetGm) {
        new Message(content: content.trim(), target: targetGm, smoker: smoker).save()
        statsService.logMessage(content, targetGm, smoker)
    }

    def findMessagesFor(int count, GmType target) {
        def unread = Message.findAllByTargetAndRead(target, false).reverse()
        def all = Message.findAllByTarget(target).reverse()
        def messages = all.take(count)

        messages.addAll(unread)
        messages.unique()
        messages.sort() { a, b -> b.id <=> a.id }

        return messages
    }
}
