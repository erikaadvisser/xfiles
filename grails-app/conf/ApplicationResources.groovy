modules = {
    bootstrap {
        resource url: [dir: 'css', file: 'bootstrap.min.css']
        resource url: [dir: 'css', file: 'justified-nav.css']
        resource url: [dir: 'css', file: 'xfiles.css']

        resource url: [dir: 'js', file: 'jquery-1.11.0.js']
        resource url: [dir: 'js', file: 'bootstrap.min.js']
        resource url: [dir: 'js', file: 'x-files.js']
    }
}