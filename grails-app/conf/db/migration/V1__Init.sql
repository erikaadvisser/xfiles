-- Create tables

CREATE TABLE agent (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  version bigint(20) NOT NULL,
  name varchar(100) NOT NULL,
  agency varchar(10) NOT NULL,
  infiltration int(11) NOT NULL,
  persuasion int(11) NOT NULL,
  science int(11) NOT NULL,
  turn_order int(11) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

