import org.n1.xfiles.StatsService
import org.n1.xfiles.TurnService
import org.n1.xfiles.model.Constants
import org.n1.xfiles.model.GmType
import org.n1.xfiles.model.MissionType
import org.n1.xfiles.model.domain.Agent
import org.n1.xfiles.model.domain.Message
import org.n1.xfiles.model.domain.Mission
import org.n1.xfiles.model.domain.Smoker

import javax.sql.DataSource

import static org.n1.xfiles.model.Agency.*
import static org.n1.xfiles.model.MissionColor.*
import static org.n1.xfiles.model.MissionStatus.ACTIVE
import static org.n1.xfiles.model.MissionType.SMOKER

class BootStrap {

    DataSource dataSource
    TurnService turnService
    StatsService statsService


    def init = { servletContext ->
//        System.out.println("datasource = " + dataSource)
//        Flyway flyway = new Flyway(dataSource: dataSource)
//        flyway.clean()
//        flyway.init()
//        flyway.migrate()

        initData()

        turnService.start()

    }

    private void initData() {
        int smokerCount = Smoker.count()
        if (smokerCount == 0) {
            Smoker italian = new Smoker(name: "The Italian", shortName: "Italian", level: 1, cssColor: "#330000", glyphicon: "glyphicon-screenshot", balance: new BigDecimal(Constants.SMOKER_START_MONEY)).save()
            Smoker wong = new Smoker(name: "Mr Wong", shortName: "Wong", level: 1, cssColor: "#000033", glyphicon: "glyphicon-eye-close", balance: new BigDecimal(Constants.SMOKER_START_MONEY)).save()
            Smoker scientist = new Smoker(name: "The Scientist", shortName: "Scientist", level: 1, cssColor: "#330033", glyphicon: "glyphicon-magnet", balance: new BigDecimal(Constants.SMOKER_START_MONEY)).save()
            Smoker dimitri = new Smoker(name: "Dimitri", shortName: "Dimitri", level: 1, cssColor: "#003333", glyphicon: "glyphicon-flash", balance: new BigDecimal(Constants.SMOKER_START_MONEY)).save()
            Smoker technocrat = new Smoker(name: "The Technocrat", shortName: "Technocrat", level: 1, cssColor: "#003300", glyphicon: "glyphicon-cog", balance: new BigDecimal(Constants.SMOKER_START_MONEY)).save()
//            Smoker none = new Smoker(name: "None", shortName: "None", level: 1, cssColor: "#ffffff", glyphicon: "", balance: BigDecimal.ZERO).save()

            Agent mulder = new Agent(name: "Fox Mulder", agency: FBI, turnOrder: 1, science: 2, persuasion: 1, infiltration: 3).save()
            Agent scully = new Agent(name: "Dana Scully", agency: FBI, turnOrder: 4, science: 3, persuasion: 2, infiltration: 1).save()
            Agent adrik = new Agent(name: "Adrik Petrov", agency: KGB, turnOrder: 2, science: 1, persuasion: 3, infiltration: 2).save()
            Agent anouska = new Agent(name: "Anouska Vasilev", agency: KGB, turnOrder: 5, science: 3, persuasion: 2, infiltration: 1).save()
            Agent dinorah = new Agent(name: "Dinorah Liss", agency: MOSSAD, turnOrder: 3, science: 2, persuasion: 1, infiltration: 3).save()
            Agent janko = new Agent(name: "Janko Shameel", agency: MOSSAD, turnOrder: 6, science: 1, persuasion: 3, infiltration: 2).save()
            System.out.println("Created 6 agents")

            Mission italian1 = new Mission(missionOrder: 1, name: "Biolab 1/2", smoker: italian, color: YELLOW, status: ACTIVE).save()
            Mission italian2 = new Mission(missionOrder: 2, name: "Biolab 2/2", smoker: italian, color: WHITE).save()
            Mission italian3 = new Mission(missionOrder: 3, name: "Virus 1/3", smoker: italian, color: BLUE).save()
            Mission italian4 = new Mission(missionOrder: 4, name: "Virus 2/3", smoker: italian, color: BROWN).save()
            Mission italianS = new Mission(missionOrder: 5, name: "Italian Smoker", smoker: italian, color: NONE, type: SMOKER).save()
            Mission italianF = new Mission(missionOrder: 6, name: "Virus 3/3", smoker: italian, color: WHITE).save()

            italian1.nextMission = italian2
            italian3.nextMission = italian4

            Mission wong1 = new Mission(missionOrder: 1, name: "UFO site 1/2", smoker: wong, color: BROWN, status: ACTIVE).save()
            Mission wong2 = new Mission(missionOrder: 2, name: "UFO site 2/2", smoker: wong, color: BLUE).save()
            Mission wongS = new Mission(missionOrder: 3, name: "Wong Smoker", smoker: wong, color: NONE, type: SMOKER).save()
            Mission wong3 = new Mission(missionOrder: 4, name: "Stargate 1/3", smoker: wong, color: WHITE).save()
            Mission wong4 = new Mission(missionOrder: 5, name: "Stargate 2/3", smoker: wong, color: RED).save()
            Mission wongF = new Mission(missionOrder: 6, name: "Stargate 3/3", smoker: wong, color: GREEN).save()

            wong1.nextMission = wong2
            wong3.nextMission = wong4

            Mission scientist1 = new Mission(missionOrder: 1, name: "Microwave 1/2", smoker: scientist, color: GREEN, status: ACTIVE).save()
            Mission scientist2 = new Mission(missionOrder: 2, name: "Microwave 2/2", smoker: scientist, color: RED).save()
            Mission scientistS = new Mission(missionOrder: 3, name: "Scientist Smoker", smoker: scientist, color: NONE, type: SMOKER).save()
            Mission scientist3 = new Mission(missionOrder: 4, name: "Onkala 1/3", smoker: scientist, color: GREEN).save()
            Mission scientist4 = new Mission(missionOrder: 5, name: "Onkala 2/3", smoker: scientist, color: YELLOW).save()
            Mission scientistF = new Mission(missionOrder: 6, name: "Onkala 3/3", smoker: scientist, color: BLUE).save()

            scientist1.nextMission = scientist2
            scientist3.nextMission = scientist4

            Mission dimitriS = new Mission(missionOrder: 1, name: "Dimitri Smoker", smoker: dimitri, color: NONE, type: SMOKER, status: ACTIVE).save()
            Mission dimitri1 = new Mission(missionOrder: 2, name: "Navy Secrets 1/2", smoker: dimitri, color: GREEN).save()
            Mission dimitri2 = new Mission(missionOrder: 3, name: "Navy Secrets 2/2", smoker: dimitri, color: YELLOW).save()
            Mission dimitri3 = new Mission(missionOrder: 4, name: "The Bloop 1/3", smoker: dimitri, color: RED).save()
            Mission dimitri4 = new Mission(missionOrder: 5, name: "The Bloop 2/3", smoker: dimitri, color: GREEN).save()
            Mission dimitriF = new Mission(missionOrder: 6, name: "The Bloop 3/3", smoker: dimitri, color: BROWN).save()

            dimitri1.nextMission = dimitri2
            dimitri3.nextMission = dimitri4

            Mission technocratS = new Mission(missionOrder: 1, name: "Technocrat Smoker", smoker: technocrat, color: NONE, type: SMOKER, status: ACTIVE).save()
            Mission technocrat1 = new Mission(missionOrder: 2, name: "Hubble Eyes 1/2", smoker: technocrat, color: WHITE).save()
            Mission technocrat2 = new Mission(missionOrder: 3, name: "Hubble Eyes 2/2", smoker: technocrat, color: RED).save()
            Mission technocrat3 = new Mission(missionOrder: 4, name: "Nibiru 1/3", smoker: technocrat, color: BROWN).save()
            Mission technocrat4 = new Mission(missionOrder: 5, name: "Nibiru 2/3", smoker: technocrat, color: BLUE).save()
            Mission technocratF = new Mission(missionOrder: 6, name: "Nibiru 3/3", smoker: technocrat, color: YELLOW).save()

            technocrat1.nextMission = technocrat2
            technocrat3.nextMission = technocrat4


            Mission SchemeTechnocrat = new Mission(missionOrder: 7, name: "Nuclear Waste", smoker: technocrat, color: BROWN).save()
            Mission SchemeScientist = new Mission(missionOrder: 7, name: "GeneticalIdentical", smoker: scientist, color: RED).save()

            Mission priorityFBI = new Mission(missionOrder: 7, name: "Priority FBI", smoker: null, color: NONE).save()
            Mission priorityKGB = new Mission(missionOrder: 7, name: "Priority KGB", smoker: null, color: NONE).save()
            Mission priorityMossad = new Mission(missionOrder: 7, name: "Priority Mossad", smoker: null, color: NONE).save()
            Mission punishmentFBI = new Mission(missionOrder: 7, name: "Punishment FBI", smoker: null, color: NONE).save()
            Mission punishmentKGB = new Mission(missionOrder: 7, name: "Punishment KGB", smoker: null, color: NONE).save()
            Mission punishmentMossad = new Mission(missionOrder: 7, name: "Punishment Mossad", smoker: null, color: NONE).save()

            Mission storyFBI = new Mission(missionOrder: 7, name: "Story Mossad", smoker: null, color: NONE).save()
            Mission storyKGB = new Mission(missionOrder: 7, name: "Story Mossad", smoker: null, color: NONE).save()
            Mission storyMossad = new Mission(missionOrder: 7, name: "Story Mossad", smoker: null, color: NONE).save()


            Mission local4Science = new Mission(missionOrder: 1, name: "Seraphim", smoker: null, type: MissionType.LOCAL, color: LOCAL, status: ACTIVE).save();
            Mission local4Persuasion = new Mission(missionOrder: 1, name: "Duane Barry", smoker: null, type: MissionType.LOCAL, color: LOCAL, status: ACTIVE).save();
            Mission local4Infiltration = new Mission(missionOrder: 1, name: "Hermit psychic", smoker: null, type: MissionType.LOCAL, color: LOCAL, status: ACTIVE).save();

            Mission local5Science = new Mission(missionOrder: 2, name: "Bermuda triangle", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local5Persuasion = new Mission(missionOrder: 2, name: "Nagasaki's truth", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local5Infiltration = new Mission(missionOrder: 2, name: "The Fragments", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();

            Mission local6Science = new Mission(missionOrder: 3, name: "Nazi legacy", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local6Persuasion = new Mission(missionOrder: 3, name: "End of the world", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local6Infiltration = new Mission(missionOrder: 3, name: "Arctic base", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();

            Mission local7Science = new Mission(missionOrder: 4, name: "The quarantine", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local7Persuasion = new Mission(missionOrder: 4, name: "The Human Genome Adjustment Project", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local7Infiltration = new Mission(missionOrder: 4, name: "Disposed hybrids", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();

            Mission local8Science = new Mission(missionOrder: 5, name: "White noise message", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local8Persuasion = new Mission(missionOrder: 5, name: "Chemtrails", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();
            Mission local8Infiltration = new Mission(missionOrder: 5, name: "Biochip", smoker: null, type: MissionType.LOCAL, color: LOCAL).save();

            new Message(content: "Start the game, good luck game master!", target: GmType.BOARD).save()
            new Message(content: "Start the game, good luck broker!", target: GmType.BROKER).save()
        }
    }

    def destroy = {
    }

}
