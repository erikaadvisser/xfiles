<%@ page import="org.n1.xfiles.model.MissionResolution" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
    <g:form method="post">
        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${missions}" var="candidate">
                        <label class="btn btn-default ${candidate.id == mission?.id ? 'active' : ''}">
                            <input type="radio" name="missionId" value="${candidate.id}" ${candidate.id == mission?.id ? 'checked' : ''}>
                            <span class="label label-default" style="background-color: ${candidate.color.cssName}">${candidate.name}</span>
                        </label>
                    </g:each>
                </div>
            </div>
            <br/>

            <div class="col-md-2 text-center">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="candidate">
                        <label class="btn btn-default btn-checkbox ${(agentsChosen?.id.contains(candidate.id)) ? 'active' : ''}">
                            <g:checkBox name="agentId" checked="${agentsChosen?.id.contains(candidate.id)}" value="${candidate.id}"/><span class="label label-default ${candidate.agency}">${candidate.name}</span>
                        </label>
                    </g:each>
                </div>
                Completer(s)
            </div>

            <div class="col-md-5">
                <div class="btn-group btn-group-lg script-radio" data-toggle="buttons">
                    <g:each in="${[MissionResolution.SUCCESS, MissionResolution.FAILED, MissionResolution.EXPOSED, MissionResolution.EXPOSE_PREVENTED]}" var="candidate">
                        <label class="btn btn-default btn-radio ${candidate == resolution ? 'active' : ''}">
                        <input type="radio" name="resolution" value="${candidate}" ${candidate == resolution ? 'checked' : ''}>${candidate.description} </input>
                        </label>
                    </g:each>
                </div>
                <br>
                <br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" id="vp_min">VP -</span>
                            <input type="text" class="form-control" value="${vp}" id="vp" name="vp">
                            <span class="input-group-addon" id="vp_plus">+</span>
                        </div>
                        <br>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" id="panic_min">&nbsp;<i class="glyphicon glyphicon-fire"></i> -</span>
                            <input type="text" class="form-control" value="${panic}" id="panic" name="panic">
                            <span class="input-group-addon" id="panic_plus">+</span>
                        </div>
                        <br>

                        <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                            <label class="btn btn-default ${backstory ? 'active' : ''}">
                                <input type="checkbox" name="backstory" value="true" ${backstory ? 'checked' : ''}>Backstory received</input>
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-2 text-center">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="candidate">
                        <label class="btn btn-default ${candidate.id == blocker?.id ? 'active' : ''}">
                        <input type="radio" name="blockerId" value="${candidate.id}" ${(candidate.id == blocker?.id) ? 'checked' : ''}>
                        <span class="label label-default ${candidate.agency}">${candidate.name}</span>
                        </input>
                    </label>
                    </g:each>
                </div>
                Blocker
            </div>

            <div class="col-md-2">
                <p>&nbsp;</p>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-md-12 text-right">
                <span class="h4 text-primary">Complete mission &nbsp;</span>
                <g:actionSubmit action="missionCompleteSubmit" class="btn btn-primary btn-lg" value="Submit"/>
            </div>
        </div>
        <g:if test="${flash.message}">
            <br/>

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;
                            </span><span class="sr-only">Close</span>
                        </button>
                        ${flash.message}
                    </div>
                </div>
            </div>
        </g:if>
    </g:form>
</div>
<script>
    window.onload = function () {
        $('.btn').button();

        function modify(fieldId, change) {
            var asText = $(fieldId).val();
            var amount = parseInt(asText);
            amount += change;
            $(fieldId).val(amount);
        }

        $("#vp_plus").click(function () {
            modify("#vp", 1);
        });
        $("#vp_min").click(function () {
            modify("#vp", -1);
        });
        $("#panic_plus").click(function () {
            modify("#panic", 1);
        });
        $("#panic_min").click(function () {
            modify("#panic", -1);
        });

        $(".btn-checkbox").click(function (event) {
            var checkbox = $("input", event.currentTarget);
            var checked = checkbox.attr('checked');
            if (checked) {
                checkbox.removeAttr('checked');
            }
            else {
                checkbox.attr('checked', 'checked');
            }
        });
    }


</script>
</body>
</html>