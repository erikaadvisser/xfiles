<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
    <g:form method="post">
        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="candidate">
                        <label class="btn btn-default ${candidate.id == agent.id ? 'active' : ''}">
                        <input type="radio" name="agentId" value="${candidate.id}" ${(agent.id == candidate.id) ? 'checked' : ''}>
                        <span class="label label-default ${candidate.agency}">${candidate.name}</span>
                        </input>
                    </label>
                    </g:each>
                </div>
            </div>

            <div class="col-md-10">

                <div class="row">
                    <div class="col-md-12 text-center">
                        <h4>Agent caused panic</h4>
                    </div>
                </div>
                <g:if test="${flash.message}">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="alert alert-info alert-dismissable" id="flashMessage">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                ${flash.message}
                            </div>
                        </div>
                    </div>
                    <br/>
                </g:if>
                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4 xfield">Panic</div>

                            <div class="input-group input-group-lg">
                                <span class="input-group-addon" id="panic_min">-</span>
                                <g:textField name="panic" value="${panic}" class="form-control" id="panic"/>
                                <span class="input-group-addon" id="panic_plus">+</span>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4 xfield">Location</div>

                            <div class="input-group input-group-lg">
                                <g:textField name="location" value="${location}" class="form-control" id="location"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <g:actionSubmit action="agentCausedPanicSubmit" value="Submit" class="btn btn-primary btn-lg"/>
            </div>
        </div>
    </g:form>
</div>
<script>
    window.onload = function () {
        $("#panic_plus").click(function () {
            xfiles.modify("#panic", 1);
        });
        $("#panic_min").click(function () {
            xfiles.modify("#panic", -1);
        });
    }
</script>
</body>
</html>