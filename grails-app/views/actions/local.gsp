<%@ page import="org.n1.xfiles.model.MissionResolution" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
    <g:form method="post">
        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${missions}" var="candidate">
                        <label class="btn btn-default ${candidate.id == mission?.id ? 'active' : ''}">
                            <input type="radio" name="missionId" value="${candidate.id}" ${candidate.id == mission?.id ? 'checked' : ''}>
                            <span class="label label-default" style="background-color: ${candidate.color.cssName}">${candidate.name}</span>
                        </label>
                    </g:each>
                    <g:if test="${missions.isEmpty()}">
                        <label class="btn btn-default" disabled>
                            <input type="radio" name="alternate" value="none" disabled>
                            <span class="label label-default">None available</span>
                        </label>
                    </g:if>
                </div>
            </div>
            <br/>

            <div class="col-md-2 text-center">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="candidate">
                        <label class="btn btn-default btn-checkbox ${(agentsChosen?.id.contains(candidate.id)) ? 'active' : ''}">
                            <g:checkBox name="agentId" checked="${agentsChosen?.id.contains(candidate.id)}" value="${candidate.id}"/><span class="label label-default ${candidate.agency}">${candidate.name}</span>
                        </label>
                    </g:each>
                </div>
                Completer(s)
            </div>

            <div class="col-md-5">
                <div class="btn-group btn-group-lg script-radio" data-toggle="buttons">
                    <g:each in="${[MissionResolution.SUCCESS, MissionResolution.FAILED]}" var="candidate">
                        <label class="btn btn-default btn-radio ${candidate == resolution ? 'active' : ''}">
                        <input type="radio" name="resolution" value="${candidate}" ${candidate == resolution ? 'checked' : ''}>${candidate.description} </input>
                        </label>
                    </g:each>
                </div>
                <br>
                <br>

                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" id="vp_min">VP -</span>
                            <input type="text" class="form-control" value="${vp}" id="vp" name="vp">
                            <span class="input-group-addon" id="vp_plus">+</span>
                        </div>
                        <br>

                        <div class="input-group input-group-lg">
                            <span class="input-group-addon" id="budget_min">&nbsp;$ -&nbsp;</span>
                            <input type="text" class="form-control" value="${budget}" id="budget" name="budget">
                            <span class="input-group-addon" id="budget_plus">+</span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-md-12 text-right">
                <span class="h4 text-primary">Process Local Mission &nbsp; &nbsp; &nbsp;</span>
                <g:actionSubmit action="localSubmit" class="btn btn-primary btn-lg" value="Submit"/>
            </div>
        </div>
        <g:if test="${flash.message}">
            <br/>

            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;
                            </span><span class="sr-only">Close</span>
                        </button>
                        ${flash.message}
                    </div>
                </div>
            </div>
        </g:if>
    </g:form>
</div>
<script>
    window.onload = function () {
        $('.btn').button();

        $("#vp_plus").click(function () {
            xfiles.modify("#vp", 1);
        });
        $("#vp_min").click(function () {
            xfiles.modify("#vp", -1);
        });
        $("#budget_plus").click(function () {
            xfiles.modify("#budget", 1);
        });
        $("#budget_min").click(function () {
            xfiles.modify("#budget", -1);
        });
    }
</script>
</body>
</html>