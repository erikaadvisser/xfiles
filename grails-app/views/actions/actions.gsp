<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div class="row">
    <div class="col-md-10">
        <g:link action="missionComplete" class="btn btn-info">Complete story mission</g:link>
        <g:link action="missionProcessEvidence" class="btn btn-info">Process Evidence</g:link>
        <g:link action="raid" class="btn btn-info">Process Raid</g:link>
    </div>
</div>
<br/>

<div class="row">
    <div class="col-md-10">
        <g:link action="local" class="btn btn-info">Process local mission</g:link>
        <g:link action="coverUp" class="btn btn-info">Cover up complete</g:link>
    </div>
</div>
<br/>

<div class="row">
    <div class="col-md-10">
        <g:link action="agentCausedPanic" class="btn btn-info">Agents cause panic</g:link>
    </div>
</div>
<br/>

</body>
</html>