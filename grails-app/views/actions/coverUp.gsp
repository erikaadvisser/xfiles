<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
    <div class="row">
        <div class="col-md-12 text-center">
            <h4>Agents cover up a mission</h4>
        </div>
    </div>
    <g:form method="post">
        <div class="row">

            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${missions}" var="candidate">
                        <label class="btn btn-default ${mission?.id == candidate.id ? 'active' : ''}">
                        <input type="radio" name="missionId" value="${candidate.id}" ${mission?.id == candidate.id ? 'checked' : ''}>
                        <span class="label label-default" style="background-color: ${candidate.color.cssName}">${candidate.name}</span>
                        </input>
                    </label>
                    </g:each>
                </div>
            </div>
            <br/>

            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${["FBI", "KGB", "Mossad"]}" var="candidate">
                        <label class="btn btn-default ${agency.name == candidate ? 'active' : ''}">
                        <input type="radio" name="agency" value="${candidate}" ${(agency.name == candidate) ? 'checked' : ''}>
                        <span class="label label-default ${candidate.toUpperCase()}">${candidate}</span>
                        </input>
                    </label>
                    </g:each>
                </div>
            </div>

            <div class="col-md-4">
                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="vp_min">VP -</span>
                    <input type="text" class="form-control" value="${vp}" id="vp" name="vp">
                    <span class="input-group-addon" id="vp_plus">VP +</span>
                </div>
                <br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="budget_min">$ -&nbsp;</span>
                    <input type="text" class="form-control" value="${budget}" id="budget" name="budget">
                    <span class="input-group-addon" id="budget_plus">&nbsp;$ +</span>
                </div>
                <br>

            </div>

        </div>

        <div class="row">
            <div class="col-md-12 text-right">
                <g:actionSubmit action="coverUpSubmit" value="Submit" class="btn btn-primary btn-lg"/>
            </div>
        </div>
        <g:if test="${flash.message}">
            <br/>

            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="alert alert-info alert-dismissable" id="flashMessage">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        ${flash.message}
                    </div>
                </div>
            </div>
        </g:if>
    </g:form>
</div>
</body>
</html>