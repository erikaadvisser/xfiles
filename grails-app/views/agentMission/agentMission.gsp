<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
    <g:if test="${flash.message}">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-warning alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert">
                        <span aria-hidden="true">&times;
                        </span><span class="sr-only">Close</span>
                    </button>
                    ${flash.message}
                </div>
            </div>
        </div>
    </g:if>
    <g:form method="post">
        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${missions}" var="mission">
                        <label class="btn btn-default">
                            <input type="radio" name="mission" value="${mission.id}"><span class="label label-default" style="background-color: ${mission.color.cssName}">${mission.name}</span>
                        </label>
                    </g:each>
                </div>
            </div>
            <br/>

            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="aa">
                        <label class="btn btn-default btn-checkbox ${(aa.id == agent.id) ? 'active' : ''}">
                            <g:checkBox name="agent" checked="${aa.id == agent.id}" value="${aa.id}"/><span class="label label-default ${aa.agency}">${aa.name}</span>
                        </label>
                    </g:each>
                </div>
            </div>

            <div class="col-md-4">
                <div class="btn-group btn-group-lg script-radio" data-toggle="buttons">
                    <label class="btn btn-default btn-radio">
                        <input type="radio" name="resolution" id="option1" value="filed">File</input>
                    </label>
                    <label class="btn btn-default btn-radio script-radio">
                        <input type="radio" name="resolution" id="option2" value="embezzled">Embezzle</input>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="resolution" id="option3" value="exposed">Expose</input>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="resolution" id="option4" value="failed">Fail</input>
                    </label>
                </div>
                <br>
                <br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="vp_min">VP -</span>
                    <input type="text" class="form-control" value="0" id="vp" name="vp">
                    <span class="input-group-addon" id="vp_plus">VP +</span>
                </div>
                <br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon" id="budget_min">$ -&nbsp;</span>
                    <input type="text" class="form-control" value="0" id="budget" name="budget">
                    <span class="input-group-addon" id="budget_plus">&nbsp;$ +</span>
                </div>
                <br>

                <div class="input-group input-group-lg">
                    <span class="input-group-addon">Panic</span>
                    <input type="text" class="form-control" value="0" id="panic" name="panic">

                </div>
                <br>

                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <label class="btn btn-default">
                        <input type="checkbox" name="backstory" value="true">Backstory received
                    </label>
                </div>
            </div>

            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="1"><span class="label label-default" style="background-color: #330000">The Italian <span class="glyphicon glyphicon-screenshot"/></span>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="5"><span class="label label-default" style="background-color: #003300">Technocrat <span class="glyphicon glyphicon-cog"/></span>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="2"><span class="label label-default" style="background-color: #000033">Mr Wong <span class="glyphicon glyphicon-eye-close"/></span>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="4"><span class="label label-default" style="background-color: #003333">Dimitri <span class="glyphicon glyphicon-flash"/></span>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="3"><span class="label label-default" style="background-color: #330033">The Scientist <span class="glyphicon glyphicon-magnet"/></span>
                    </label>
                    <label class="btn btn-default script-radio">
                        <input type="radio" name="smoker" value="0"><span class="text-muted">(none)</span>
                    </label>
                </div>
            </div>

            <div class="col-md-2">
                <p>&nbsp;</p>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-md-2">&nbsp;
            %{--<select class="form-control">--}%
            %{--<option SELECTED value="select">Select Raid</option>--}%
            %{--<option value="FBI">FBI</option>--}%
            %{--<option value="KGB">KGB</option>--}%
            %{--<option value="MOSSAD">Mossad</option>--}%
            %{--</select>--}%
            %{--<select class="form-control">--}%
            %{--<option selected value="select">Select Target</option>--}%
            %{--<option value="1">UFO Sitings 1</option>--}%
            %{--<option value="2">Virus 1</option>--}%
            %{--<option value="3">Virus 2</option>--}%
            %{--<option value="4">Onkala 1</option>--}%
            %{--<option value="5">Onkala 2</option>--}%
            %{--<option value="6">Nibiru 1</option>--}%
            %{--</select>--}%
            </div>

            <div class="col-md-8">&nbsp;
            </div>

            <div class="col-md-2">
                <g:actionSubmit action="save" class="btn btn-primary btn-lg" value="Submit"/>
            </div>
        </div>
    </g:form>
</div>
<script>
    window.onload = function () {
        $('.btn').button();

        function modify(fieldId, change) {
            var asText = $(fieldId).val();
            var amount = parseInt(asText);
            amount += change;
            $(fieldId).val(amount);
        }

        $("#vp_plus").click(function () {
            modify("#vp", 1);
        });
        $("#vp_min").click(function () {
            modify("#vp", -1);
        });
        $("#budget_plus").click(function () {
            modify("#budget", 1);
        });
        $("#budget_min").click(function () {
            modify("#budget", -1);
        });

        $(".btn-checkbox").click(function (event) {
            var checkbox = $("input", event.currentTarget);
            var checked = checkbox.attr('checked');
            if (checked) {
                checkbox.removeAttr('checked');
            }
            else {
                checkbox.attr('checked', 'checked');
            }
        });
    }


</script>
</body>
</html>