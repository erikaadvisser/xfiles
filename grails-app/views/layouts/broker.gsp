<%@ page import="org.n1.xfiles.model.Page" %>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <title>X-Files Command game</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <g:layoutHead/>
    <r:layoutResources/>
</head>

<body class="broker">
<div class="container">
    <div class="masthead">
        <g:img dir="images" file="780px-The_X-Files_logo.svg.png" style="height: 120px; position: fixed; right:10px; top:50px"/>
        <ul class="nav nav-justified">
            <g:each in="${Page}" var="menu">
                <g:if test="${!menu.boardGame}">
                    <g:if test="${page == menu}">
                        <li class="active">
                    </g:if>
                    <g:else>
                        <li>
                    </g:else>
                    <g:link controller="${menu.controller}" action="${menu.action}">${menu.name}</g:link>
                    </li>
                </g:if>
            </g:each>
            <li>
                <g:link controller="${Page.BOARD_HOME.controller}" action="${Page.BOARD_HOME.action}">Board</g:link>
            </li>
        </ul>
    </div>

    <p/>

    <div class="panel panel-default">
        <div class="panel-body">
            <g:layoutBody/>
        </div>
    </div>
</div>
<r:layoutResources/>
</body>
</html>
