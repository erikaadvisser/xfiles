<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="broker"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div id="page-body">
<br/>

<div class="row">
    <div class="col-md-10">&nbsp;
        <a href="brokerMission?status=ACTIVE" class="btn btn-info">Active</a>
        <a href="brokerMission?status=PENDING" class="btn btn-info">Pending</a>
        <a href="brokerMission?status=INACTIVE" class="btn btn-info">Inactive</a>
        <a href="brokerMission?status=COMPLETED" class="btn btn-info">Completed</a>
        <a href="brokerMission?status=ALL" class="btn btn-info">All</a>
    </div>
</div>
<g:form method="post">
    <div class="col-md-12">
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Smoker</th>
                <th>Mission</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${missions}" var="mission">
                <tr>
                    <td><h4>${mission.smoker?.name}</h4></td>
                    <td><h4><span class="label label-default" style="background-color: ${mission.color.cssName}">${mission.name}</span></h4></td>
                    <td>${mission.status}</td>
                    <td>
                        <g:each in="${mission.actions}" var="action">
                            <a href="action?id=${mission.id}&command=${action}" class="btn btn-default">${action}</a>
                        </g:each>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>

    </div>
</g:form>
</div>
</body>
</html>