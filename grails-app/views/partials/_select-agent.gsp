<div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
    <g:each in="${agents}" var="aa">
        <label class="btn btn-default ${aa.id == agent.id? 'active' : ''}" >
            <g:checkBox name="agent_selected[${aa.id}]"/><span class="label label-default ${aa.agency.name}">${aa.name}</span>
        </label>
    </g:each>
</div>