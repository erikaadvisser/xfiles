<div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
    <label class="btn btn-default">
        <input type="radio" name="smoker-4"><span class="label label-default" style="background-color: #330000">The Italian <span class="glyphicon glyphicon-screenshot"/></span>
    </label>
    <label class="btn btn-default">
        <input type="radio" name="smoker-2"><span class="label label-default" style="background-color: #003300">Technocrat <span class="glyphicon glyphicon-cog"/></span>
    </label>
    <label class="btn btn-default">
        <input type="radio" name="smoker-2"><span class="label label-default" style="background-color: #000033">Mr Wong <span class="glyphicon glyphicon-eye-close"/></span>
    </label>

    <label class="btn btn-default">
        <input type="radio" name="smoker-1"><span class="label label-default" style="background-color: #003333">Dimitri <span class="glyphicon glyphicon-flash"/></span>
    </label>
    <label class="btn btn-default">
        <input type="radio" name="smoker-5"><span class="label label-default" style="background-color: #330033">The Scientist <span class="glyphicon glyphicon-magnet"/></span>
    </label>
    <label class="btn btn-default">
        <input type="radio" name="smoker-0"><span class="text-muted">(none)</span>
    </label>
</div>