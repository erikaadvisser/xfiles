<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
    N</head>

<body>
<div id="page-body">
    <g:form method="post">
        <input type="hidden" name="id" value="${agent.id}"/>

        <div class="row">
            <div class="col-md-2">
                <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                    <g:each in="${agents}" var="aa">
                        <label class="btn btn-default ${aa.id == agent.id ? 'active' : ''}" onclick="window.location.href = 'stats?id=${aa.id}'">
                            <g:checkBox name="agent_selected[${aa.id}]"/><span class="label label-default ${aa.agency}">${aa.name}</span>
                        </label>
                    </g:each>
                </div>
            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-4 xfield">Science</div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="science_min">-</span>
                        <g:textField name="science" value="${agent.science}" class="form-control" id="science"/>
                        <span class="input-group-addon" id="science_plus">+</span>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-md-4 xfield">Persuasion</div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="persuasion_min">-</span>
                        <g:textField name="persuasion" value="${agent.persuasion}" class="form-control" id="persuasion"/>
                        <span class="input-group-addon" id="persuasion_plus">+</span>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-md-4 xfield">Infiltration</div>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon" id="infiltration_min">-</span>
                        <g:textField name="infiltration" value="${agent.infiltration}" class="form-control" id="infiltration"/>
                        <span class="input-group-addon" id="infiltration_plus">+</span>
                    </div>
                    <br/>

                    <div class="row">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input type="hidden" name="_fired" value=""/>
                        <input type="hidden" name="_hiding" value=""/>

                        <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                            <label class="btn btn-default ${agent.fired ? 'active' : ''}">
                                <input type="checkbox" name="fired" value="true" ${agent.fired ? 'checked' : ''}>Fired
                            </label>
                        </div>

                        <div class="btn-group-vertical btn-group-lg" data-toggle="buttons">
                            <label class="btn btn-default ${agent.hiding ? 'active' : ''}">
                                <input type="checkbox" name="hiding" value="true" ${agent.hiding ? 'checked' : ''}>Hiding
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10">&nbsp;</div>

            <div class="col-md-2">
                <g:actionSubmit action="save" value="Submit" class="btn btn-primary btn-lg"/>
            </div>
        </div>
    </g:form>
</div>
<script>
    window.onload = function () {


        $("#science_plus").click(function () {
            xfiles.modify("#science", 1);
        });
        $("#science_min").click(function () {
            xfiles.modify("#science", -1);
        });
        $("#persuasion_plus").click(function () {
            xfiles.modify("#persuasion", 1);
        });
        $("#persuasion_min").click(function () {
            xfiles.modify("#persuasion", -1);
        });
        $("#infiltration_plus").click(function () {
            xfiles.modify("#infiltration", 1);
        });
        $("#infiltration_min").click(function () {
            xfiles.modify("#infiltration", -1);
        });
    }
</script>
</body>
</html>
