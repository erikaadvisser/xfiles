<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="${layout}"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<g:form>
    <div class="row">
        <div class="col-md-10">
            <g:textField name="content" class="form-control"></g:textField>
        </div>
    </div>
    <br/>
    <br/>

    <div class="row">
        <div class="col-md-10">
            <g:actionSubmit action="${command}" value="Submit" class="btn btn-primary btn-lg"/>
        </div>
    </div>
</g:form>

</body>
</html>