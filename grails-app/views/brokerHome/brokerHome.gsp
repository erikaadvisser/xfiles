<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="broker"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div class="row">
    <div class="col-md-10">
        <table class="table">
            <thead>
            <tr>
                <th>Time</th>
                <th>Message</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${messages}" var="message">
                <tr class="${message.read ? '' : 'danger'}">
                    <td>${message.entryTime.toString("HH:mm")}</td>
                    <td width="90%">${message}</td>
                    <td><a href="${message.command}Message?id=${message.id}" class="btn btn-default"><span class="${message.icon}"/></a></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

</body>
</html>