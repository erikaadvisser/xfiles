<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<div class="row">
    <div class="col-md-10">
        <g:link action="start" class="btn btn-default btn-lg" style="width: 104px;">Start</g:link>
    </div>
</div>
<g:form method="post">
    <div class="row">
        <div class="col-md-7">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Agent name</th>
                    <th>Player name</th>
                    <th>Turn order</th>
                </tr>
                </thead>
                <tbody>
                <g:each in="${agentInstanceList}" status="i" var="agent">
                    <tr class="">
                        <td><span class="label label-default ${agent.agency}">${agent.name}</span></td>
                        <td><g:textField name="agent[${agent.id}].ocName" value="${agent.ocName}"></g:textField></td>
                        <td><g:textField name="agent[${agent.id}].turnOrder" value="${agent.turnOrder}"></g:textField></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
        </div>
    </div>

    <div class="row">
        <div class="col-md-7">
            %{--<g:link action="save" class="btn btn-default">Save</g:link>--}%
            <g:actionSubmit action="save" class="btn btn-default" value="Save"/>
        </div>
    </div>
</g:form>
</body>
</html>