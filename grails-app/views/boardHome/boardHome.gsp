<%@ page contentType="text/html;charset=UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="board"/>
    <r:require modules="bootstrap"/>
</head>

<body>
<g:if test="${flash.message}">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;
                    </span><span class="sr-only">Close</span>
                </button>
                ${flash.message}
            </div>
        </div>
    </div>
</g:if>
<div class="row">
    <div class="col-md-2">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color: ${agent.agency.cssColor}; color: white;"><Strong>${agent.agency}<br>${agent.name}</Strong></div>

            <div id="turnTime" class="panel-body"></div>
        </div>
    </div>

    <div class="col-md-10">
        <p>

        <div class="col-md-2">
            <h4>Panic: 1</h4>
        </div>

        <div class="col-md-2">
            <h4>Round: ${round}</h4>
        </div>

        <div class="col-md-3">
            <h4>Total time: <span id="gameTime"></span></h4>
        </div>
    </p>
        <br>
        <br>

        <p>

        <div class="col-md-10">
            <g:link action="previousPlayer" class="btn btn-default btn-lg" style="width: 104px;">Previous</g:link>
            <g:link action="pause" class="btn ${paused ? 'btn-primary' : 'btn-default'} btn-lg" style="width: 104px;">Pause</g:link>
            <g:link action="nextPlayer" class="btn btn-default btn-lg" style="width: 104px;">Next</g:link>
        </div>
    </p>
    </div>
</div>

<div class="row">
    <div class="col-md-2">&nbsp;</div>

    <div class="col-md-10">
        <table class="table">
            <thead>
            <tr>
                <th>Time</th>
                <th>Message</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${messages}" var="message">
                <tr class="${message.read ? '' : 'danger'}">
                    <td>${message.entryTime.toString("HH:mm")}</td>
                    <td width="90%"><h4>${message}</h4></td>
                    <td><a href="${message.command}Message?id=${message.id}" class="btn btn-default"><span class="${message.icon}"/></a></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>

<script>
    window.onload = function () {
        function hhmmss(sec_num) {
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
            var seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            var time = hours + ':' + minutes + ':' + seconds;
            return time;
        }

        var turnSeconds = ${turnSeconds};
        var gameSeconds = ${gameSeconds};

        function updateTime() {
            turnSeconds += 1;
            $("#turnTime").text(hhmmss(turnSeconds));
            gameSeconds += 1;
            $("#gameTime").text(hhmmss(gameSeconds));
        }

        updateTime();
        var pause = ${paused};
        if (!pause) {
            setInterval(function () {
                updateTime();
            }, 1000);
        }
    }
</script>
</body>
</html>