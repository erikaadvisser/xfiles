package org.n1.xfiles

import grails.test.mixin.TestFor
import org.n1.xfiles.model.domain.Agent
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Agent)
class AgentSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
    }
}
