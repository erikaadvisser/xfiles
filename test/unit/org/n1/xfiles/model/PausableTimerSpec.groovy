package org.n1.xfiles.model

import spock.lang.Specification

class PausableTimerSpec extends Specification {

    PausableTimer timer

    def setup() {
        timer = new PausableTimer()
    }

    def "Timer runs for two seconds without pause"() {
        when:
        Thread.sleep(2004) // running 2 seconds
        int secondsElapsed = timer.secondsSoFar();

        then:
        secondsElapsed == 2
    }

    def "Timer that runs for one second and is then paused for one second"() {
        when:
        Thread.sleep(1102) // running 1 second
        timer.togglePause()
        Thread.sleep(1102) // paused 1 second
        int secondsElapsed = timer.secondsSoFar();

        then:
        secondsElapsed == 1
    }

    def "Timer that runs for one second and is then paused for one second then unpaused and then runs for one second"() {
        when:
        Thread.sleep(1102) // running 1 second
        timer.togglePause()
        Thread.sleep(1102) // paused 1 second
        timer.togglePause()
        Thread.sleep(1102) // running 1 second
        timer.togglePause()
        int secondsElapsed = timer.secondsSoFar();

        then:
        secondsElapsed == 2
    }

    def "Timer that starts paused for one second,  runs for one second and is then paused for one second "() {
        when:
        timer.togglePause()
        Thread.sleep(900) // paused 1 second
        timer.togglePause()
        Thread.sleep(900) // running 1 second
        timer.togglePause()
        Thread.sleep(900) // paused 1 second
        int secondsElapsed = timer.secondsSoFar();

        then:
        secondsElapsed == 1
    }
}
