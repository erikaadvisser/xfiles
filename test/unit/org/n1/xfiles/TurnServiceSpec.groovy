package org.n1.xfiles

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(TurnService)
class TurnServiceSpec extends Specification {

    TurnService turnService;

    def setup() {
        turnService = new TurnService();
    }

    def cleanup() {
    }

    void "test something"() {
        when:
        int a = 1;
        a = a + 1;

        then:
        a == 2
    }
}
