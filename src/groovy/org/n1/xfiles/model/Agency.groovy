package org.n1.xfiles.model

/**
 * This enum defines the agencies in the game.
 */
public enum Agency {

    FBI("FBI", "#428bca"),
    KGB("KGB", "#f0ad4e"),
    MOSSAD("Mossad", "#47a447");

    public final String name
    public final String cssColor

    Agency(String name, String cssColor) {
        this.name = name
        this.cssColor = cssColor
    }

    static Agency findByName(String name) {
        return Agency.valueOf(name.toUpperCase())
    }
}