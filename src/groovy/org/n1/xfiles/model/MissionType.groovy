package org.n1.xfiles.model


public enum MissionType {
    STORY, SMOKER, LOCAL, BACKSTORY, SCHEME, PUNNISHMENT, PRIORITY
}