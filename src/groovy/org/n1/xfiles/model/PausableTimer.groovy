package org.n1.xfiles.model

import org.joda.time.LocalTime
import org.joda.time.Period

/**
 * This class provides a timer that can be paused and resumed.
 */
class PausableTimer {

    private LocalTime startTime = LocalTime.now()
    private boolean paused = false
    private int totalPausedSeconds = 0
    private LocalTime pauseStartTime = null

    void togglePause() {
        if (paused) {
            unPause()
        }
        else {
            pauseStartTime = LocalTime.now();
            paused = true;
        }
    }

    int secondsSoFar() {
        Period period = new Period(startTime, LocalTime.now())
        int millis = period.toStandardDuration().millis
        int secondsTotal = (millis + 500)/ 1000;
        return secondsTotal - totalPausedSeconds - secondsThisPause()
    }

    private int secondsThisPause() {
        if (paused) {
            Period period = new Period(pauseStartTime, LocalTime.now())
            int millis = period.toStandardDuration().millis
            return (millis + 500)/ 1000
        }
        else {
            return 0
        }
    }

    def unPause() {
        if (paused) {
            totalPausedSeconds += secondsThisPause()
            pauseStartTime = null
            paused = false
        }
    }
}
