package org.n1.xfiles.model


enum MissionColor {

    BLUE("#269abc"),
    BROWN("saddlebrown"),
    GREEN("green"),
    RED("orangered"),
    WHITE("#cccccc"),
    YELLOW("#cccc00"),
    NONE("#C4A6C4"),
    LOCAL("darkkhaki")

    public final String cssName;

    MissionColor(String cssName) {
        this.cssName = cssName
    }
}
