package org.n1.xfiles.model


enum Skill {
    SCIENCE,
    PERSUASION,
    INFILTRATION
}
