package org.n1.xfiles.model


public enum MissionStatus {

    INACTIVE, ACTIVE, PENDING, COMPLETED

}