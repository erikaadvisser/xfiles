package org.n1.xfiles.model


public enum MissionResolution {
    SUCCESS("Success"),
    FAILED("Failed"),
    FILED("Filed"),
    EMBEZZLED("Embezzled"),
    EXPOSED("Exposed"),
    EXPOSE_PREVENTED("Expose foiled")

    String description

    MissionResolution(String description) {
        this.description = description
    }
}